#-------------------------------------------------------------------------------
# MIT License
#
# Copyright (c) 2024 University College Cork (UCC)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Panagiotis Kyziropoulos - Author
#     Dimitrios Bikoulis - Co-author
#
#-------------------------------------------------------------------------------

from app import app
import app.services as sv
import app.models as ml
from flask import jsonify, request, render_template
from werkzeug.utils import secure_filename

# ------ API Calls ------ 

# Renders the html homepage.
@app.route('/')
def index():
    """
    Renders the html homepage.
    """
    try:
        link1 = "http://"+app.config["SERVICE_IP"]+":"+str(app.config["SERVICE_PORT"])+"/apidocs/"
        link2 = "http://"+app.config["TESTCASE_IP"]+":"+str(app.config["TESTCASE_PORT"])+"/"
        link3 = "https://aiod-broker.readthedocs.io/en/latest/introduction.html"
        return render_template('index.html',link_api_documentation=link1,link_test_usecase=link2,link_service_documentation=link3)

    except:
        app.logger.error('Error: README.md file was not found.')
        return "Error: File was not found!", 500

# The user can fetch the translated metadata from the AIoD.
@app.route('/api/datasets_metadata', methods=['POST'])
def get_dataset() -> tuple[str, int]:
    """
    The user can fetch the translated metadata from the AIoD.
    
    Returns:
        tuple: The requested datasets' metadata from the AIoD platform.
    """
    try:
        dataset_ids = request.json
        dataset_metadata = sv.fetch_dataset(dataset_ids["datasets"])
        return jsonify(dataset_metadata), 200
    except:
        return jsonify({'message': 'Error: API server is down.'}), 500

# The user can upload metadata in datamite's format and receive the translated version of the metadata in AIoD's format.
@app.route('/api/translate_metadata', methods=['POST'])
def translate_metadata() -> tuple[str, int]:
    """
    The user can upload metadata in datamite's format and receive the translated version of the metadata in AIoD's format.
    
    Returns:
        tuple: The uploaded metadata in AIoD's format.
    """
    try:
        # receives metadata from api call 
        metadata = request.json

        # read AIoD (portal) glassary
        glossary = sv.readAIODglossary()

        # get EU portal metadata
        portal_metadata=sv.translate2AIoD(metadata,glossary)

        return jsonify(portal_metadata), 200
    except:
        return jsonify({'message': 'Error: Metadata structure was Incorrect.'}), 422

# Login to the AIoD platform.
@app.route('/api/login_eu_portal', methods=['POST'])
def login() -> tuple[str, int]:
    """
    Login to the AIoD platform.
    
    Returns:
        tuple: The username and user-id if the login procedure is successful, an error message otherwise.
    """
    try:
        # receives user credentials
        user = request.json
        response = sv.user_exists(app.config["user"], user["username"])

        if (response["status"]):
            if (response["object"]["password"]==user["password"]):
                return jsonify({"username":user["username"],"id":response["object"]["id"]}), 200
            else:
                return jsonify({'message': 'Error: Credentials were Incorrect.'}), 422
        else:
            token = sv.login(user["username"],user["password"])
            if (token == None):
                return jsonify({'message': 'Error: Credentials were Incorrect.'}), 422
            else:
                id = sv.generate_string()
                app.config["user"].append({
                        "username": user["username"],
                        "password": user["password"],
                        "id": id,
                        "token": token
                })
                return jsonify({"username":user["username"],"id":id}), 200
    except:
        return jsonify({'message': 'Error: The authentication server is down.'}), 500

# Publishes datasets' metadata to the AIoD platform.
@app.route('/api/publish_eu_portal', methods=['POST'])
def publish() -> tuple[str, int]:
    """
    Publishes datasets' metadata to the AIoD platform.
    
    Returns:
        tuple: The AIoD's API responses to the calls.
    """
    try:
        # receives metadata
        data = request.json
        
        # check if the user exists
        response = sv.user_exists(app.config["user"], data["user"]["username"])
        if (response["status"]):
            if (response["object"]["id"] == data["user"]["id"]):

                res = sv.publish(data,response["object"]["token"])

                # log publishing to logging service
                ids = [id['identifier'] for id in res]
                sv.logging(ids)

                return jsonify(res), 200

        return jsonify({'message': 'Error: Metadata structure was Incorrect.'}), 422
    except:
        return jsonify({'message': 'Error: API server is down.'}), 500