#-------------------------------------------------------------------------------
# MIT License
#
# Copyright (c) 2024 University College Cork (UCC)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Panagiotis Kyziropoulos - Author
#     Dimitrios Bikoulis - Co-author
#
#-------------------------------------------------------------------------------

from app.models import db, Dataset
from datetime import datetime
from app import app
import random
import string
import json
import requests
from dateutil import parser
from keycloak import KeycloakOpenID, KeycloakAuthenticationError, ConnectionManager, KeycloakPostError
from jsonpath_ng import parse 


# -------- Tools -------- 

# Returns a KeycloakOpenID object
def _connect_keycloak() -> KeycloakOpenID:
    """
    Returns a KeycloakOpenID object.

    Returns:
        KeycloakOpenID: A KeycloakOpenID object.
    """
    return KeycloakOpenID(
        server_url=app.config["API_URL"]+"/aiod-auth/",
        client_id="aiod-api-swagger",
        realm_name="aiod",
    )

# Connects to Keycloak server.
def keycloak_openid() -> KeycloakOpenID:
    """
    Connects to Keycloak server.

    Returns:
        KeycloakOpenID: A KeycloakOpenID object.
    """
    global _keycloak_openid
    if _keycloak_openid is None:
        _keycloak_openid = _connect_keycloak()
    return _keycloak_openid

_keycloak_openid: KeycloakOpenID | None = None

# Generates a random ID string.
def generate_string(length:int=32) -> str:
    """
    Generates a random ID string.

    Args:
        length (int): The number of characters that the ID will have (default value = 32).

    Returns:
        str: The ID in string format.
    """
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


# ------ Functions ------ 

# Returns a dictionary that contains the access/refresh tokens.
def login(username:str, password:str) -> dict | None:
    """
    Returns a dictionary that contains the access/refresh tokens.
    
    Args:
        username (str): The username.
        password (str): The password.

    Returns:
        dict: The access/refresh token.
    """
    try:
        token = keycloak_openid().token(username, password)

        return token 
    except:
        return None    

# Checks if the user exists in the config users table.
def user_exists(users:list, username:str) -> dict:
    """
    Checks if the user exists in the config users table.
    
    Args:
        user (list): The list of users' records.
        username (str): The username to be checked whether is in the list.

    Returns:
        dict: A dictionary with a field called status that is True if the user exists and False otherwise, 
              along with a field called object that contains the user's record in the database.
    """
    for user in users:
        if user["username"] == username:
            return {"status":True,"object":user} 
    return {"status":False,"object":{}} 

# Reads the glossary file for the AIOD/DATAMITE translation.
def readAIODglossary() -> list:
    """
    Reads the glossary file for the AIOD/DATAMITE translation.

    Returns:
        list: The glossary object.

    """
    
    f = open('files/datamite_aiod_glossary.json')
    glossary = json.load(f)

    return glossary

# Translates the input metadata according to the provided AIoD's glossary.
def translate2AIoD(metadata:dict, glossary:list) -> list:
    """
    Translates the input metadata according to the provided AIoD's glossary.
    
    Args:
        metadata (dict): The metadata of Datamite's schema.
        glossary (dict): The glossary for the translation.

    Returns:
        list: A list of dictionaries with a field called payload that contains a list of dictionaries. 
              The nested dictionaries have the following fields:
              - attribute (str): The name of the attribute.
              - type (str): the type of the attribute.
              - value (depending on the attribute): the value of the attribute.
              - mandatory (boolean): Whether it is mandatory or not.
              - description (str): A description of the attribute.
              - example (depending on the attribute): An example value of the attribute.
    """
    new_metadata = []

    for dataset in metadata["entity"]["datasets"]:
        payload=[]
        for rule in glossary:

            attribute = rule['attribute']
            translation = rule['translation']
            default_value = rule['default']
            value_type = rule['type']
            mandatory = rule['mandatory']
            description = rule['description']
            example = rule['example']

            if (translation==""):
                val = default_value
            else:

                jsonpath_expr = parse(translation)
                match = jsonpath_expr.find(dataset)

                if match:
                    value = match[0].value
                    val = value
                else:
                    value = default_value
                    val = default_value

            if (value_type == "datetime"):
                val=convert_date_to_iso_format(val)

            if (attribute == "distribution"):
                val['content_url'] = val.pop('accessUrl')
                val=[val]

            payload.append({
                "attribute": attribute,
                "type" : value_type,
                "value": val,
                "mandatory" : mandatory,
                "description" : description,
                "example" : example
            })

        new_metadata.append({
            "payload":payload,
        })

    return new_metadata

# Converts a date string to ISO 8601 format.
def convert_date_to_iso_format(date_str:str) -> str:
    """
    Converts a date string to ISO 8601 format.
    
    Args:
        date_str (str): The date in string format.
        
    Returns:
        str: The date in ISO 8601 format.
    """
    dt = parser.parse(date_str)    
    formatted_date = dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
    
    return formatted_date

# Handles publish functionality depending on whether the dataset is new or old.
def publish(data:dict, token:dict) -> list: 
    """
    Handles publish functionality depending on whether the dataset is new or old.
    
    Args:
        data (dict): A dicitionary with fields datasets which contains a list of the translated-to-AIoD metadata for the different datasets
                     and user which contains a dicitonary with field username:user and id:user-id.
        token (dict): A dictionary that contains the access/refresh tokens.

    Returns:
        list: Contains the responses in dictionary format of the POST method for each dataset.
    """
    responses=[]
    for i in range(len(data["datasets"])):

        single_dataset = data["datasets"][i]
        payload=parse2payload(single_dataset)
        dataset =  get_platform_id_by_dataset_name(payload["name"])
        if (dataset == None):
            resp = send_dataset_to_platform(payload,token)
        else:
            resp = update_dataset_to_platform(payload,token,dataset)
        responses.append(resp)

    return responses

# Fetches the datasets (in AIoD metadata schema) from the AIoD by using the AIoD API.
def fetch_dataset(dataset_ids:list) -> dict:
    """
    Fetches the datasets (in AIoD metadata schema) from the AIoD by using the AIoD API.
    
    Args:
        dataset_ids (list): Containts the ids of the datasets to be fetched.
        
    Returns:
        dict: A dictionary with a field response which contains a list of the AIoD metadata for each requested dataset.
    """
    resp=[]
    for i in range(len(dataset_ids)):
        path="/datasets/v1/"+str(dataset_ids[i])+"?schema=aiod"
        url = app.config["API_URL"]+path

        response = requests.get(url)
        resp.append(response.json())

    return {"response":resp}

# Adds a new dataset entry to the AIoD platform and the local database.
def send_dataset_to_platform(payload:dict,token:dict) -> dict:
    """
    Adds a new dataset entry to the AIoD platform and the local database.
    
    Args:
        payload (dict): A dictionary with the tranlated metadata in the AIoD schema.
        token (dict): A dictionary contains the access/refresh token.

    Returns:
        dict: A dictionary with fields status and the AIoD's dataset identifier.
    """
    # define platform and API URL
    platform_name="example"
    path="/datasets/v1"
    url = app.config["API_URL"]+path

    # generate new id
    dataset_identifier = generate_string(length=63)

    # add entries to platform identifier to payload
    payload["platform"] = platform_name
    payload["platform_resource_identifier"] = dataset_identifier

    # define token header
    headers = {
        "Authorization": "Bearer "+token["access_token"]
    }

    # send post request to eu platform
    response = requests.post(url, json=payload, headers=headers)
    resp=response.json()
    resp["status"]="Created"

    # add dataset to database
    add_dataset(dataset_identifier, payload["name"], resp["identifier"])

    return resp

# Updates a pre-existing dataset entry to the AIoD portal and the local database.
def update_dataset_to_platform(payload:dict,token:dict,dataset:dict) -> dict:
    """
    Updates a pre-existing dataset entry to the AIoD portal and the local database.
    
    Args:
        payload (dict): A dictionary with the tranlated metadata in the AIoD schema.
        token (dict): A dictionary that containts the access/refresh token.
        dateset (dict): The metadata to be updated.

    Returns:
        dict: A dictionary with fields status and the AIoD's dataset identifier.
    """
    resp=[]
    
    # define platform and API URL
    platform_name="example"
    path="/datasets/v1/"+dataset["platform_id"]
    url = app.config["API_URL"]+path

    # generate new id
    dataset_identifier = dataset["id"]

    # add entries to platform identifier to payload
    payload["platform"] = platform_name
    payload["platform_resource_identifier"] = dataset_identifier

    # define token header
    headers = {
        "Authorization": "Bearer "+token["access_token"]
    }

    # send put request to eu platform
    response = requests.put(url, json=payload, headers=headers)
    resp={
        "identifier":dataset["platform_id"],
        "status":"Updated"
    }

    # update modify_date entry in local db
    update_modify_date_entry(dataset_identifier)

    return resp

# Parses the received list of dicitonaries to the appropriate AIoD payload format.
def parse2payload(data:list) -> dict:
    """
    Parses the received list of dicitonaries to the appropriate AIoD payload format.
    
    Args:
        data (list): Contains dictionaries with the following format:
                    - attribute (str): The name of the attribute.
                    - type (str): the type of the attribute.
                    - value (depending on the attribute): the value of the attribute.
                    - mandatory (boolean): Whether it is mandatory or not.
                    - description (str): A description of the attribute.
                    - example (depending on the attribute): An example value of the attribute.
        
    Returns:
        dict: A dictionary in the AIoD payload format.
    """
    payload={}
    for i in range(len(data)):

        keys = data[i]["attribute"].split(".")
        value = data[i]["value"]
        set_nested_value(payload, keys, value)
    return payload

# Parses the received dictionary to the appropriate AIoD payload format.
def set_nested_value(d:dict, keys:list, value):
    """
    Parses the received dictionary to the appropriate AIoD payload format.
    
    Args:
        d (dict): The return dictionary (default empty).
        keys (list): A list of attributes.
        values (str | dict | list | float | int | boolean): The target value.
    """
    for key in keys[:-1]:
        if key not in d:
            d[key] = {}
        d = d[key]
    d[keys[-1]] = value

# Adds a dataset to the database.
def add_dataset(identifier:str, Dataset_name:str, platform_identifier:str):
    """
    Adds a dataset to the database.
    
    Args:
        identifier (str): The identifier of the dataset.
        Dataset_name (str): The name of the dataset.
        platform_identifier (str): The identifier of the AIoD platform.
    """
    # get date-time on string format
    now=datetime.now().strftime("%d/%m/%Y, %H:%M:%S")

    dataset = Dataset(id=identifier,name=Dataset_name,platform_id=platform_identifier,create_date=now,modify_date=now)
    db.session.add(dataset)
    db.session.commit()

# Removes a dataset from the database using the ID.
def remove_dataset(dataset_id:str) -> int:
    """
    Removes a dataset from the database using the ID.
    
    Args:
        dataset_id (str): The identifier of the dataset.
        
    Returns:
        int: 1 if the dataset existed and was successfully deleted, 0 otherwise. 
    """
    dataset = Dataset.query.get(dataset_id)
    if dataset:
        db.session.delete(dataset)
        db.session.commit()
        return 1
    else:
        return 0

# Returns the platform_id and the id of a dataset within the database with a specific name.
def get_platform_id_by_dataset_name(name:str) -> dict | None:
    """
    Returns the platform_id and the id of a dataset within the database with a specific name.
    
    Args: 
        name (str): The name of the dataset.
    
    Returns:
        dict: A dictionary with two fields platformd_id and id. 
    """
    # Query the dataset by name
    dataset = db.session.query(Dataset).filter(Dataset.name == name).first()

    # If the dataset exists, return its ID
    if dataset:
        return {"platform_id":dataset.platform_id,"id":dataset.id}
    else:
        return None

# Updates the modify_date entry of a dataset in the database to the latest datetime (datetime:now()).
def update_modify_date_entry(dataset_id:str):
    """
    Updates the modify_date entry of a dataset in the database to the latest datetime (datetime:now()).
    
    Args:
        dataset_id (str): the id of the dataset.
    """
    # get date-time on string format
    now=datetime.now().strftime("%d/%m/%Y, %H:%M:%S")

    # Query the dataset by ID
    dataset = db.session.query(Dataset).filter(Dataset.id == dataset_id).first()
    
    if dataset:
        # Update the modify_date field
        dataset.modify_date = now
        
        # Commit the changes to the database
        db.session.commit()

# Integration with logging tool.
def logging(dataset_ids:list):
    """
    Integration with the logging tool.
   
    Args:
        dataset_id (list): A list with the id of the datasets.
   
    Returns:
        dict: A dictionary
    """
   
    # The URL for the post request
    path="/metadataLog/publish"
    API_URL = app.config["LOGGING_SERVICE"] + path
 
    # Fetch the metadata from in AIoD Schema.
    response = fetch_dataset(dataset_ids=dataset_ids)["response"]
   
    # Create the payload for the post request.
    payload = {
        "endpointUsed" : API_URL,
        "catalogId": generate_string(36),
        "catalogName": 'name_of_the_catalogue',
        "providerId": generate_string(36),
        "providerName": "test_company",
        "userToken": "user_token",
        "userId": "dc57d527-0964-4146-8c4b-e50a526759fc",
        "userName": "userX",
        "createdAt": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.111Z"),
        "datasets" : response
    }
   
    try:
        # Post request
        resp = requests.post(API_URL, json=payload)
    except:
        print("Warning: The logging service is down.")
