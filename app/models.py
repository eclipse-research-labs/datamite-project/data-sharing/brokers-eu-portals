#-------------------------------------------------------------------------------
# MIT License
#
# Copyright (c) 2024 University College Cork (UCC)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Panagiotis Kyziropoulos - Author
#     Dimitrios Bikoulis - Co-author
#
#-------------------------------------------------------------------------------

from flask_sqlalchemy import SQLAlchemy

# Define Database
db = SQLAlchemy()

# Database Classes
class Dataset(db.Model):
    """
    This class creates a Dataset object in the SQLite database.

    Attributes:
    - id (str): The primary key for the dataset. 
    - name (str): Name of the dataset.
    - platform_id (str): Identifier for the AIoD platform.
    - create_date (str): Date when the dataset was created.
    - modify_date (str): Date when the dataset was modified. 
    
    """
    id = db.Column(db.String(255), primary_key=True)
    name = db.Column(db.String(255))
    platform_id = db.Column(db.String(63))
    create_date = db.Column(db.String(255))
    modify_date = db.Column(db.String(255))


    def __repr__(self):
        return f"<Dataset {self.id}>"