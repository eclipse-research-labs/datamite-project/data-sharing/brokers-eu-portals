// -------------------------------------------------------------------------------
// MIT License
//
// Copyright (c) 2024 University College Cork (UCC)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//  
//   
// SPDX-License-Identifier: MIT
//   
// Contributors:
//    Panagiotis Kyziropoulos - Author
//    Dimitrios Bikoulis - Co-author
// 
// -------------------------------------------------------------------------------
const data1 = 
{
    "entity": {
    "entityID": "catalog-001",
    "entityQualifiedName": "Weather Data Product",
    "entityDescription": "A catalog of daily weather datasets.",
    "entityOwner": "Meteo Agency",
    "entityCreatedAt": "Meteo Agency",
    "entityUpdatedAt": "2024-06-03",
    "entityUpdatedBy": "Meteo Agency",
    "tags":["weather"],
    "terms": [
        {
          "id": "term-001",
          "qualifiedName": "temperature"
        },
        {
          "id": "term-002",
          "qualifiedName": "humidity"
        }
    ],
    "datasets": [
        {
            "Dataset": {
                "identifier": "dataset-004-2024-06-01",
                "title": "Weather Data for 2024-06-01",
                "description": "Weather data for June 1, 2024.",
                "creator": "Weather Station Alpha",
                "created": "2024-06-02",
                "distribution": [
                {
                    "Distribution": {
                    "accessUrl": "https://example.com/data/weather-2024-06-01.csv",
                    "byteSize": 512,
                    "mediaType": "text/csv",
                    "format": "CSV",
                    "checksum": "checksum1234"
                    }
                }
                ],
                "qualityMeasurements": [
                {
                    "QualityMeasurement": {
                    "identifier": "qm-004-2024-06-01-01",
                    "title": "Data Completeness for 2024-06-01",
                    "value": 99.9,
                    "generatedAtTime": "2024-06-02T00:00:00Z",
                    "wasGeneratedBy": "QualityCheckerTool",
                    "isMeasurementOf": "Completeness",
                    "parameters": {
                        "threshold": 100,
                        "unit": "%"
                    }
                    }
                },
                {
                    "QualityMeasurement": {
                    "identifier": "qm-004-2024-06-01-02",
                    "title": "Data Accuracy for 2024-06-01",
                    "value": 98.5,
                    "generatedAtTime": "2024-06-02T00:00:00Z",
                    "wasGeneratedBy": "AccuracyCheckerTool",
                    "isMeasurementOf": "Accuracy",
                    "parameters": {
                        "threshold": 100,
                        "unit": "%"
                    }
                    }
                }
                ],
                "database": {
                    "Database": {
                        "identifier": "db-004",
                        "dbType": "PostgreSQL",
                        "tables": [
                        {
                            "Table": {
                            "identifier": "table-004-2024-06-01",
                            "schema": "public",
                            "isPrivate": false,
                            "encoding": "UTF-8",
                            "columns": [
                                {
                                    "Column": {
                                        "identifier": "Time",
                                        "dataType": "timestamp",
                                        "position": 1,
                                        "numRecords": 1440,
                                        "isPrivate": false
                                    }
                                },
                                {
                                "Column": {
                                    "identifier": "LAT",
                                    "dataType": "FLOAT",
                                    "position": 2,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                },
                                {
                                "Column": {
                                    "identifier": "LONG",
                                    "dataType": "FLOAT",
                                    "position": 3,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                },
                                {
                                "Column": {
                                    "identifier": "TEMP",
                                    "dataType": "FLOAT",
                                    "position": 4,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                }
                            ]
                            }
                        }
                        ]
                    }
                }
            }
        },
        {
            "Dataset": {
                "identifier": "dataset-004-2024-06-02",
                "title": "Weather Data for 2024-06-02",
                "description": "Weather data for June 2, 2024.",
                "creator": "Weather Station Alpha",
                "created": "2024-06-03",
                "distribution": [
                {
                    "Distribution": {
                    "accessUrl": "https://example.com/data/weather-2024-06-02.csv",
                    "byteSize": 512,
                    "mediaType": "text/csv",
                    "format": "CSV",
                    "checksum": "checksum5678"
                    }
                }
                ],
                "qualityMeasurements": [
                {
                    "QualityMeasurement": {
                    "identifier": "qm-004-2024-06-02-01",
                    "title": "Data Completeness for 2024-06-02",
                    "value": 99.8,
                    "generatedAtTime": "2024-06-03T00:00:00Z",
                    "wasGeneratedBy": "QualityCheckerTool",
                    "isMeasurementOf": "Completeness",
                    "parameters": {
                        "threshold": 100,
                        "unit": "%"
                    }
                    }
                },
                {
                    "QualityMeasurement": {
                    "identifier": "qm-004-2024-06-02-02",
                    "title": "Data Accuracy for 2024-06-02",
                    "value": 98.7,
                    "generatedAtTime": "2024-06-03T00:00:00Z",
                    "wasGeneratedBy": "AccuracyCheckerTool",
                    "isMeasurementOf": "Accuracy",
                    "parameters": {
                        "threshold": 100,
                        "unit": "%"
                    }
                    }
                }
                ],
                "database": {
                    "Database": {
                        "identifier": "db-004",
                        "dbType": "PostgreSQL",
                        "tables": [
                        {
                            "Table": {
                            "identifier": "table-004-2024-06-02",
                            "schema": "public",
                            "isPrivate": false,
                            "encoding": "UTF-8",
                            "columns": [
                                {
                                    "Column": {
                                        "identifier": "Time",
                                        "dataType": "timestamp",
                                        "position": 1,
                                        "numRecords": 1440,
                                        "isPrivate": false
                                    }
                                },
                                {
                                "Column": {
                                    "identifier": "LAT",
                                    "dataType": "FLOAT",
                                    "position": 2,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                },
                                {
                                "Column": {
                                    "identifier": "LONG",
                                    "dataType": "FLOAT",
                                    "position": 3,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                },
                                {
                                "Column": {
                                    "identifier": "TEMP",
                                    "dataType": "FLOAT",
                                    "position": 4,
                                    "numRecords": 1440,
                                    "isPrivate": false
                                }
                                }
                            ]
                            }
                        }
                        ]
                    }
                }
            }
        }
    ]
    },
    "glossary": [
      {
        "id": "term-001",
        "qualifiedName": "temperature",
        "name": "Temperature",
        "shortDescription": "The degree of heat present in the atmosphere.",
        "longDescription": "Temperature is a measure of the average kinetic energy of the particles in a system. It is one of the principal parameters in thermodynamics.",
        "createdAt": "2024-06-01T00:00:00Z",
        "updatedAt": "2024-06-01T00:00:00Z"
      },
      {
        "id": "term-002",
        "qualifiedName": "humidity",
        "name": "Humidity",
        "shortDescription": "The amount of water vapor in the atmosphere.",
        "longDescription": "Humidity is a measure of the amount of water vapor present in the air. It can significantly affect weather conditions and human comfort.",
        "createdAt": "2024-06-01T00:00:00Z",
        "updatedAt": "2024-06-01T00:00:00Z"
      }
    ]
}

const data2 = 
{
    "entity": {
        "entityID": "catalog-002",
        "entityQualifiedName": "Customer Support Data Product",
        "entityDescription": "A catalog of daily customer support datasets.",
        "entityOwner": "Business A",
        "entityCreatedAt": "Business A",
        "entityUpdatedAt": "2024-06-03",
        "entityUpdatedBy": "Business A",
        "tags":["customers"],
        "terms": [
            {
                "id": "term-003",
                "qualifiedName": "ticket_id"
            },
            {
                "id": "term-004",
                "qualifiedName": "response_time"
            }
        ],
        "datasets": [
            {
                "Dataset": {
                    "identifier": "dataset-005-2024-06-01",
                    "title": "Customer Support Data for 2024-06-01",
                    "description": "Customer support data for June 1, 2024.",
                    "creator": "Support Team Alpha",
                    "created": "2024-06-02",
                    "distribution": [
                        {
                            "Distribution": {
                                "accessUrl": "https://example.com/data/support-2024-06-01.csv",
                                "byteSize": 256,
                                "mediaType": "text/csv",
                                "format": "CSV",
                                "checksum": "checksum7890"
                            }
                        }
                    ],
                    "qualityMeasurements": [
                        {
                            "QualityMeasurement": {
                                "identifier": "qm-005-2024-06-01-01",
                                "title": "Data Completeness for 2024-06-01",
                                "value": 99.5,
                                "generatedAtTime": "2024-06-02T00:00:00Z",
                                "wasGeneratedBy": "QualityCheckerTool",
                                "isMeasurementOf": "Completeness",
                                "parameters": {
                                    "threshold": 100,
                                    "unit": "%"
                                }
                            }
                        },
                        {
                            "QualityMeasurement": {
                                "identifier": "qm-005-2024-06-01-02",
                                "title": "Data Accuracy for 2024-06-01",
                                "value": 97.8,
                                "generatedAtTime": "2024-06-02T00:00:00Z",
                                "wasGeneratedBy": "AccuracyCheckerTool",
                                "isMeasurementOf": "Accuracy",
                                "parameters": {
                                    "threshold": 100,
                                    "unit": "%"
                                }
                            }
                        }
                    ],
                    "database": {
                        "Database": {
                            "identifier": "db-005",
                            "dbType": "MySQL",
                            "tables": [
                                {
                                    "Table": {
                                        "identifier": "table-005-2024-06-01",
                                        "schema": "support",
                                        "isPrivate": false,
                                        "encoding": "UTF-8",
                                        "columns": [
                                            {
                                                "Column": {
                                                    "identifier": "ticket_id",
                                                    "dataType": "INTEGER",
                                                    "position": 1,
                                                    "numRecords": 500,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "agent_id",
                                                    "dataType": "INTEGER",
                                                    "position": 2,
                                                    "numRecords": 500,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "response_time",
                                                    "dataType": "FLOAT",
                                                    "position": 3,
                                                    "numRecords": 500,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "resolution_time",
                                                    "dataType": "FLOAT",
                                                    "position": 4,
                                                    "numRecords": 500,
                                                    "isPrivate": false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        ]
    },
    "glossary": [
        {
            "id": "term-003",
            "qualifiedName": "ticket_id",
            "name": "Ticket ID",
            "shortDescription": "A unique identifier for a support ticket.",
            "longDescription": "The ticket ID is a unique number assigned to each support ticket created. It is used to track and manage the ticket throughout its lifecycle.",
            "createdAt": "2024-06-01T00:00:00Z",
            "updatedAt": "2024-06-01T00:00:00Z"
        },
        {
            "id": "term-004",
            "qualifiedName": "response_time",
            "name": "Response Time",
            "shortDescription": "The time taken to respond to a support ticket.",
            "longDescription": "Response time measures the duration between the creation of a support ticket and the first response from a support agent. It is a key metric for evaluating customer service performance.",
            "createdAt": "2024-06-01T00:00:00Z",
            "updatedAt": "2024-06-01T00:00:00Z"
        }
    ]
}

const data3 = 
{
    "entity": {
        "entityID": "catalog-003",
        "entityQualifiedName": "Financial Data Product",
        "entityDescription": "A catalog of daily financial datasets.",
        "entityOwner": "Financial Agency",
        "entityCreatedAt": "Financial Agency",
        "entityUpdatedAt": "2024-06-03",
        "entityUpdatedBy": "Financial Agency",
        "tags":["finance"],
        "terms": [
            {
                "id": "term-005",
                "qualifiedName": "stock_price"
            },
            {
                "id": "term-006",
                "qualifiedName": "trading_volume"
            }
        ],
        "datasets": [
            {
                "Dataset": {
                    "identifier": "dataset-006-2024-06-01",
                    "title": "Financial Data for 2024-06-01",
                    "description": "Financial data for June 1, 2024.",
                    "creator": "Finance Department Alpha",
                    "created": "2024-06-02",
                    "distribution": [
                        {
                            "Distribution": {
                                "accessUrl": "https://example.com/data/finance-2024-06-01.csv",
                                "byteSize": 1024,
                                "mediaType": "text/csv",
                                "format": "CSV",
                                "checksum": "checksum3456"
                            }
                        }
                    ],
                    "qualityMeasurements": [
                        {
                            "QualityMeasurement": {
                                "identifier": "qm-006-2024-06-01-01",
                                "title": "Data Completeness for 2024-06-01",
                                "value": 99.9,
                                "generatedAtTime": "2024-06-02T00:00:00Z",
                                "wasGeneratedBy": "QualityCheckerTool",
                                "isMeasurementOf": "Completeness",
                                "parameters": {
                                    "threshold": 100,
                                    "unit": "%"
                                }
                            }
                        },
                        {
                            "QualityMeasurement": {
                                "identifier": "qm-006-2024-06-01-02",
                                "title": "Data Accuracy for 2024-06-01",
                                "value": 99.1,
                                "generatedAtTime": "2024-06-02T00:00:00Z",
                                "wasGeneratedBy": "AccuracyCheckerTool",
                                "isMeasurementOf": "Accuracy",
                                "parameters": {
                                    "threshold": 100,
                                    "unit": "%"
                                }
                            }
                        }
                    ],
                    "database": {
                        "Database": {
                            "identifier": "db-006",
                            "dbType": "Oracle",
                            "tables": [
                                {
                                    "Table": {
                                        "identifier": "table-006-2024-06-01",
                                        "schema": "finance",
                                        "isPrivate": false,
                                        "encoding": "UTF-8",
                                        "columns": [
                                            {
                                                "Column": {
                                                    "identifier": "timestamp",
                                                    "dataType": "timestamp",
                                                    "position": 1,
                                                    "numRecords": 1000,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "stock_symbol",
                                                    "dataType": "VARCHAR",
                                                    "position": 2,
                                                    "numRecords": 1000,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "stock_price",
                                                    "dataType": "FLOAT",
                                                    "position": 3,
                                                    "numRecords": 1000,
                                                    "isPrivate": false
                                                }
                                            },
                                            {
                                                "Column": {
                                                    "identifier": "trading_volume",
                                                    "dataType": "INTEGER",
                                                    "position": 4,
                                                    "numRecords": 1000,
                                                    "isPrivate": false
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        ]
    },
    "glossary": [
        {
            "id": "term-005",
            "qualifiedName": "stock_price",
            "name": "Stock Price",
            "shortDescription": "The price of a single share of a company's stock.",
            "longDescription": "Stock price is the cost of purchasing a single share of a company's stock. It fluctuates based on market conditions and company performance.",
            "createdAt": "2024-06-01T00:00:00Z",
            "updatedAt": "2024-06-01T00:00:00Z"
        },
        {
            "id": "term-006",
            "qualifiedName": "trading_volume",
            "name": "Trading Volume",
            "shortDescription": "The total number of shares traded during a given period.",
            "longDescription": "Trading volume refers to the number of shares or contracts traded in a security or market during a given period. It is an important indicator of market activity and liquidity.",
            "createdAt": "2024-06-01T00:00:00Z",
            "updatedAt": "2024-06-01T00:00:00Z"
        }
    ]
}
