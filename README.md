<!-- 
-------------------------------------------------------------------------------
 MIT License

 Copyright (c) 2024 University College Cork (UCC)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 
 SPDX-License-Identifier: MIT
 
 Contributors:
     Panagiotis Kyziropoulos - Author
     Dimitrios Bikoulis - Co-author

------------------------------------------------------------------------------- 
-->
# Broker for EU Portals and Data Markets

## Module purpose

The goal of this architecture is to facilitate the seamless publication of metadata to key platforms such as the AI on Demand (AIoD), which operates via a REST API, while considering other publishing technologies as well, such as OAI-PMH Harvesters, for the integraton with different platforms. 

![architecture_Brokers](/images/arch_extended_Brokers.png)

The service documentation can be found at: https://aiod-broker.readthedocs.io/en/latest/introduction.html

# Installation Guide

## Prerequisites

Before installing this repository, the user should install the AI on Demand services located in the AI on Demand API GitHub repository. Follow these steps:

Clone the AI on Demand repository:
   ```sh
   git clone https://github.com/aiondemand/AIOD-rest-api.git
   ```
Navigate to the cloned directory:
   ```sh
   cd AIOD-rest-api
   ```
Build the Docker services using the following command:
   ```sh
   docker compose --profile examples up -d
   ```
Follow the installation instructions in the AI on Demand repository for more information.

## Repository Installation

Before installing this repository, download or clone the code from Eclipse GitLab and update the configuration files.

Clone the repository:
   ```sh
   git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/brokers-eu-portals.git
   ```
Navigate to the cloned directory:
   ```sh
   cd brokers-eu-portals
   ```

### Configuration Update

Edit the `config/development.py` or `config/production.py` file (by default, it is `development.py`):

Open the configuration file in a text editor.
Change the line:
   ```python
   API_URL = "http://127.0.0.1"
   ```
   to the appropriate AI on Demand IP. **Note:** It should not be `localhost` or `http://127.0.0.1` even if it is located locally because the services will be containerized. It should be the complete server IP.

### Installation Methods

#### Method 1: Python Environment and NodeJS Setup

##### Using Virtualenv

Create a Python virtual environment with Python 3.10:
   ```sh
   python3.10 -m venv venv
   ```
Activate the virtual environment:
   - On Windows:
     ```sh
     venv\Scripts\activate
     ```
   - On macOS/Linux:
     ```sh
     source venv/bin/activate
     ```
Install the Python requirements:
   ```sh
   pip install -r requirements.txt
   ```
Run the `app.py` service:
   ```sh
   python app.py
   ```
Navigate to the NodeJS service directory:
   ```sh
   cd example
   ```
Install the NodeJS dependencies:
   ```sh
   npm install
   ```
Start the NodeJS service:
   ```sh
   npm start
   ```

##### Using Conda

Create a Conda environment with Python 3.10:
   ```sh
   conda create -n myenv python=3.10
   ```
Activate the Conda environment:
   ```sh
   conda activate myenv
   ```
Install the Python requirements:
   ```sh
   pip install -r requirements.txt
   ```
Run the `app.py` service:
   ```sh
   python app.py
   ```
Navigate to the NodeJS service directory:
   ```sh
   cd example
   ```
Install the NodeJS dependencies:
   ```sh
   npm install
   ```
Start the NodeJS service:
   ```sh
   npm start
   ```

#### Method 2: Docker Compose

Build and run the Docker services:
   ```sh
   docker compose up --build
   ```

## Service Information

The broker service is up at [http://localhost:4000/](http://localhost:4000/)
The broker's API documentation is available at [http://localhost:4000/apidocs/](http://localhost:4000/apidocs/)
The test use case front-end is up at [http://localhost:3000/](http://localhost:3000/)
The username for login in the test use case is `user` and the password is `password`.


<!-- ### Roadmap M13-M18

The following roadmap present the set of actions to be completed in the Sprint 3 of DATAMITE, broken down by corresponding
module.

![Roadmap](/images/Roadmap_M13-M18.png "Roadmap") -->