Architecture
============

.. toctree::
   :maxdepth: 1

   context
   container
   component
   data_flow
