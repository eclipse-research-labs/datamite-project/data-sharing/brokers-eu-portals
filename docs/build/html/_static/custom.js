// custom.js
document.addEventListener("DOMContentLoaded", function () {
    // Create a new button element
    const button = document.createElement("a");
    button.textContent = "View Repository";  // Text for the button
    button.href = "https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/brokers-eu-portals";  // Link to your desired URL
    button.target = "_blank";  // Open link in a new tab
    button.style.cssText = `
        margin-left: 10px; 
        padding: 5px 10px; 
        background-color: #2980B9; 
        color: white; 
        border-radius: 4px; 
        text-decoration: none;
    `;

    // Append the button to the header
    const header = document.querySelector("div.wy-side-nav-search");
    if (header) {
        header.appendChild(button);
    }
});