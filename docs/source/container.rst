.. -------------------------------------------------------------------------------
..  MIT License
.. 
..  Copyright (c) 2024 University College Cork (UCC)
.. 
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
.. 
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
.. 
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
.. 
..  
..  SPDX-License-Identifier: MIT
..  
..  Contributors:
..      Panagiotis Kyziropoulos - Author
..      Dimitrios Bikoulis - Co-author
.. 
.. -------------------------------------------------------------------------------
Container Diagram
=================

The Container Diagram, as displayed below, presents the high-lever overview of the brokering service's functions within the DATAMITE system, in particular with the Data Governance Module. Focusing on metadata publication, the Data Governance Container interfaces with the database, extracting metadata fo feed into the brokering service. Subsequently, depending on the EU portal type, the brokering service employs distinc mechanisms: for the REST API-based plaftorms, metadata is directly pushed to the system.

.. image:: ../../images/Broker_container_diagram.png 
   :alt: System Container Diagram