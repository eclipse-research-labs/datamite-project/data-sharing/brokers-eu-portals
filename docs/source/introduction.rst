.. -------------------------------------------------------------------------------
..  MIT License
.. 
..  Copyright (c) 2024 University College Cork (UCC)
.. 
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
.. 
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
.. 
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
.. 
..  
..  SPDX-License-Identifier: MIT
..  
..  Contributors:
..      Panagiotis Kyziropoulos - Author
..      Dimitrios Bikoulis - Co-author
.. 
.. -------------------------------------------------------------------------------
Introduction
============

DATAMITE: AI on Demand Broker is a Python Flask service designed to publish metadata of datasets within the DATAMITE framework to the AI on Demand EU portal. This service utilizes a SQLite database and Flasgger for API documentation.

The service includes an example folder containing a simple Node.js GUI that demonstrates a use case of the brokering service. Authentication with the AI on Demand portal is managed via an external Keycloak server. 

The goal of this architecture is to facilitate the seamless publication of metadata to key platforms such as the AI on Demand (AIoD), which operates via a REST API, while considering other publishing technologies as well, such as OAI-PMH Harvesters, for the integration with different platforms.

.. image:: ../../images/arch_extended_Brokers.png
   :alt: Architecture Datamite