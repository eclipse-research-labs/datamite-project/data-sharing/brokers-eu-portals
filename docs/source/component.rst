.. -------------------------------------------------------------------------------
..  MIT License
.. 
..  Copyright (c) 2024 University College Cork (UCC)
.. 
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
.. 
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
.. 
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
.. 
..  
..  SPDX-License-Identifier: MIT
..  
..  Contributors:
..      Panagiotis Kyziropoulos - Author
..      Dimitrios Bikoulis - Co-author
.. 
.. -------------------------------------------------------------------------------
Component Diagram
=================

The Component diagram describes the brokering component and its four constituting modules:

* Data Consistency Checker, which ensures metadata completeness; if incomplete, users are prompted for additional information.
* Data Translation Module, which transforms DATAMITE's catalogue structure into the format required by the chosen EU portal.
* Authenticator Module, which extrablishes, authenticates and authorises connections for data publication.
* Data Integration Gateway, which houses the necessary code for network interface technologies, facilitating the seamless transfer of metadata.

.. image:: ../../images/Broker_component_diagram.png
   :alt: System Component Diagram