#-------------------------------------------------------------------------------
# MIT License
#
# Copyright (c) 2024 University College Cork (UCC)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Panagiotis Kyziropoulos - Author
#     Dimitrios Bikoulis - Co-author
#
#-------------------------------------------------------------------------------

import os
import sys
sys.path.insert(0, os.path.abspath('../..'))  # Adjust this path as needed

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'AIoD Broker'
copyright = '2024 University College Cork'
author = 'Panagiotis Kyziropoulos, Dimitrios Bikoulis'
release = '0.01'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.viewcode',
    'sphinx_autodoc_typehints',
    'sphinx.ext.napoleon',  # If you're using Google or NumPy style docstrings
    'recommonmark',
    'sphinx_copybutton',
    'sphinxcontrib.redoc',
    'sphinx_rtd_theme',  # Add this line
]

templates_path = ['_templates']
exclude_patterns = ['example/*']

html_theme = 'sphinx_rtd_theme'

html_show_sourcelink = False


html_static_path = ['_static']  # Make sure the _static folder is included

html_js_files = [
    'custom.js',  # Add your custom JavaScript file here
]

html_theme_options = {
    "collapse_navigation": False,
    "navigation_depth": 4,
    "style_external_links": True,
}