//  -------------------------------------------------------------------------------
//  MIT License
//
//  Copyright (c) 2024 University College Cork (UCC)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//
//
//  SPDX-License-Identifier: MIT
// 
//  Contributors:
//     Panagiotis Kyziropoulos - Author
//     Dimitrios Bikoulis - Co-author
//
//  ------------------------------------------------------------------------------- 
// custom.js
document.addEventListener("DOMContentLoaded", function () {
    // Create a new button element
    const button = document.createElement("a");
    button.textContent = "View Repository";  // Text for the button
    button.href = "https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/brokers-eu-portals";  // Link to your desired URL
    button.target = "_blank";  // Open link in a new tab
    button.style.cssText = `
        margin-left: 10px; 
        padding: 5px 10px; 
        background-color: #2980B9; 
        color: white; 
        border-radius: 4px; 
        text-decoration: none;
    `;

    // Append the button to the header
    const header = document.querySelector("div.wy-side-nav-search");
    if (header) {
        header.appendChild(button);
    }
});