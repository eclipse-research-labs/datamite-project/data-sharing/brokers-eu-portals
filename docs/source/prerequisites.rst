.. -------------------------------------------------------------------------------
..  MIT License
.. 
..  Copyright (c) 2024 University College Cork (UCC)
.. 
..  Permission is hereby granted, free of charge, to any person obtaining a copy
..  of this software and associated documentation files (the "Software"), to deal
..  in the Software without restriction, including without limitation the rights
..  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
..  copies of the Software, and to permit persons to whom the Software is
..  furnished to do so, subject to the following conditions:
.. 
..  The above copyright notice and this permission notice shall be included in all
..  copies or substantial portions of the Software.
.. 
..  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
..  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
..  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
..  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
..  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
..  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
..  SOFTWARE.
.. 
..  
..  SPDX-License-Identifier: MIT
..  
..  Contributors:
..      Panagiotis Kyziropoulos - Author
..      Dimitrios Bikoulis - Co-author
.. 
.. -------------------------------------------------------------------------------
Prerequisites
=============

AIoD API Installation
---------------------

Before installing this repository, the user should install the AI on Demand services located in the AI on Demand API GitHub repository. Follow these steps:

Clone the AI on Demand repository::

   git clone https://github.com/aiondemand/AIOD-rest-api.git

Navigate to the cloned directory::

   cd AIOD-rest-api

Build the Docker services using the following command::

   docker compose profile examples up -d


Follow the installation instructions in the AI on Demand repository for more information.

Repository Installation
-----------------------

The next step is to download or clone the code from Eclipse GitLab and update the configuration files.

Clone the repository::

   git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/brokers-eu-portals.git

Navigate to the cloned directory::

   cd brokers-eu-portals

Edit the ``config/development.py`` or ``config/production.py`` file (by default, it is ``development.py``):

Configuration
-------------

Open the configuration file in a text editor.
Change the line::

   API_URL = "http://127.0.0.1"

to the appropriate AI on Demand IP. **Note:** It should not be ``localhost`` or ``http://127.0.0.1`` even if it is located locally because the services will be containerized. It should be the complete server IP.



.. Before installing this repository, the user should install the AI on Demand services located in the AI on Demand API GitHub repository. Follow these steps:

.. Clone the AI on Demand repository:

.. .. code-block:: bash

..    git clone https://github.com/aiondemand/AIOD-rest-api.git

.. Navigate to the cloned directory:

.. .. code-block:: bash
..    cd AIOD-rest-api

.. Build the Docker services using the following command:

.. .. code-block:: bash
..    docker compose profile examples up -d

.. Follow the installation instructions in the AI on Demand repository for more information.

.. The next step is to download or clone the code from Eclipse GitLab and update the configuration files.
.. Clone the repository:

.. .. code-block:: bash
..    git clone https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/brokers-eu-portals.git

.. Navigate to the cloned directory:
.. .. code-block:: bash
..    cd brokers-eu-portals

.. Edit the "config/development.py" or "config/production.py" file (by default, it is "development.py"):

.. Open the configuration file in a text editor.
.. Change the line:
.. .. code-block:: python
..    API_URL = "http://127.0.0.1"

..    to the appropriate AI on Demand IP. **Note:** It should not be `localhost` or `http://127.0.0.1` even if it is located locally because the services will be containerized. It should be the complete server IP.