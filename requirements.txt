#-------------------------------------------------------------------------------
# MIT License
#
# Copyright (c) 2024 University College Cork (UCC)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# 
# SPDX-License-Identifier: MIT
# 
# Contributors:
#     Panagiotis Kyziropoulos - Author
#     Dimitrios Bikoulis - Co-author
#
#-------------------------------------------------------------------------------

numpy==1.26.4
scipy==1.12.0
scikit-learn==1.4.1.post1
Flask==3.0.2
flask-cors==4.0.1
Flask-SQLAlchemy==3.1.1
flasgger==0.9.7.1
SQLAlchemy==2.0.25
markdown2==2.4.13
waitress==3.0.0
requests==2.32.3
python-keycloak==4.1.0
jsonpath-ng==1.6.1
python-dateutil==2.9.0.post0
